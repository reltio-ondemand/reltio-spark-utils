name := "reltio-spark-utils"

version := "1.0"

organization := "com.reltio"

scalaVersion := "2.11.7"

crossScalaVersions  := Seq("2.11.7", "2.10.4")

qblClusterId := "27927"//"23876" //Id of the cluster to upload your jar

qblLibraryPath := "s3://reltio.master/libs/release-tool/" //A location on S3 to upload your jar to. Note, the location should be accessible by your Qubole account

qblRestart := true

qblJarFiles := Seq(s"${name.value}-${version.value}.jar")

qblBootstrapFile := "s3://reltio.dw/qubole/scripts/hadoop/apps_dev/attach-jars-apps-dev.sh"

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))

//ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.9.40" % "provided",
  "org.apache.spark" %% "spark-core" % "2.0.2" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.0.2" % "provided",
  "org.apache.spark" %% "spark-graphx" % "2.0.2" % "provided",

  "org.scalactic" %% "scalactic" % "2.2.6" % "test",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "org.mockito" % "mockito-all" % "1.10.19" % "test",
  "org.apache.commons" % "commons-lang3" % "3.4" % "test",
  "org.slf4j" % "slf4j-api" % "1.7.21" % "test",

  "org.scalaj" %% "scalaj-http" % "2.3.0" % "provided"
  )
  //below is a workaround to fix classpath collision for jackson
  .map(_.exclude("com.fasterxml.jackson.module", "jackson-module-scala_2.11")) ++ Seq(
    "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.4.5" % "provided"
  )

assemblyJarName in assembly := s"${name.value}-${version.value}.jar"

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

/*assemblyShadeRules in assembly := Seq(
  ShadeRule.rename("com.amazonaws.**" -> "reltio.aws.@1").inProject,
)*/

test in assembly := {}

//logLevel in assembly := Level.Debug

resolvers ++= Seq(Resolver.mavenLocal)