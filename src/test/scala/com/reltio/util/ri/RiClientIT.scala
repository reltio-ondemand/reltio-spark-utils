package com.reltio.util.ri

import java.text.SimpleDateFormat
import java.util.Calendar

import com.reltio.util.AwsClient
import com.reltio.util.auth.AuthService
import org.json4s.jackson.JsonMethods
import org.scalatest.FlatSpec

class RiClientIT extends FlatSpec{
  val user = System.getenv("reltioUsername")
  val apiKey = System.getenv("reltioKey")
  val passw = System.getenv("reltioPassword")
  val authUrl = "https://auth-stg.reltio.com/oauth/token"
  val url = "https://reltio-af-tst-01.reltio.com"
  val awsKey = System.getenv("awsKey")
  val awsSecret = System.getenv("awsSecret")

  "ListJobs" should "not fail and return a list of jobs (or empty list if none)" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.authenticate(user, passw)

    val riClient = new RiClient(url, token)
    val jobs = riClient.listJobs(true)

    //Event empty job list results in non-empty string, so it's ok
    assert(jobs.length > 0)
  }

  "MappingPath" should "produce correct location for the mapping file" in {
    val now = Calendar.getInstance().getTime()
    val format = new SimpleDateFormat("YY-MM-dd-hh:mm")
    val ts = format.format(now)

    val res = RiClient.mappingPath("test.bucket/my/input.data.csv")

    assert(res.startsWith("test.bucket/my/input.data"))
    //This is not ideal test, since it has probability to produce false positive failures
    assert(res.endsWith(s"$ts-mapping.json"))
  }

  "Upload Mapping" should "create file next to input file" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.authenticate(user, passw)

    val riClient = new RiClient(apiKey, token)
    val input = "ri.int.test/ri-client/input.csv"
    val mapping = "some mapping"
    val path = riClient.uploadMapping(input, mapping, awsKey, awsSecret)

    val awsClient = AwsClient(awsKey, awsSecret)
    val res = awsClient.downloadFile("s3://" + path)

    assert(res.equals(mapping))

    awsClient.deleteFile("s3://" + path)
  }

  "Upload File" should "create file with given location" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.authenticate(user, passw)

    val riClient = new RiClient(apiKey, token)
    val input = "ri.int.test/ri-client/input.csv"
    val content = "some content"
    val path = riClient.uploadFile(input, content, awsKey, awsSecret)

    val awsClient = AwsClient(awsKey, awsSecret)
    val res = awsClient.downloadFile("s3://" + path)

    assert(res.equals(content))

    awsClient.deleteFile("s3://" + path)
  }

  "Build DataImport Body" should "generate valid JSON" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.authenticate(user, passw)
    val riClient = new RiClient(apiKey, token)

    val content = riClient.buildDataImportBody(
      tenant = "test",
      input = "input.csv",
      mapping = "mapping.json",
      awsKey = "key",
      awsSecret = "secret",
      cluster = 10
    )
    val json = JsonMethods.parse(content)
    val pretty = JsonMethods.pretty(json)

    val expected =
      """{
        |  "name" : "Data import",
        |  "tenant" : "test",
        |  "tasks" : [ {
        |    "application" : "DataImport",
        |    "payload" : {
        |      "inputPath" : "s3n://input.csv",
        |      "inputMappingPath" : "s3n://mapping.json",
        |      "awsKey" : "key",
        |      "awsSecret" : "secret",
        |      "inactive" : "true",
        |      "dryRun" : "false"
        |    }
        |  } ],
        |  "cluster" : {
        |    "size" : 10
        |  }
        |}""".stripMargin

    assert(pretty == expected)
  }

}
