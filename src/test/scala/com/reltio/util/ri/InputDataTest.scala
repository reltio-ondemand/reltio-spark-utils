package com.reltio.util.ri

import org.scalatest.FlatSpec

class InputDataTest extends FlatSpec{
  "Input Data" should "produce correct mkString result" in {
    val id = InputData(
      header = List("f1", "f2"),
      lines = List(
        InputRow(List("v1", "v2")),
        InputRow(List("v3", "v4"))
      )
    )

    val s = id.mkString()

    val expected =
      """f1, f2
        |v1, v2
        |v3, v4""".stripMargin

    assert(s.equals(expected))
  }

}
