package com.reltio.util.ri

import org.scalatest.FlatSpec

class MappingTest extends FlatSpec{
  "Crosswalk Mapping" should "produce correct static mapping" in {
    val cw = CrosswalkMapping("testType", "testValue", dynamic = false)
    val s = cw.mkString

    val expected = "{\"type\": \"configuration/sources/testType\", \"value\": \"testValue\"}"
    assert(s.equals(expected))
  }

  it should "produce correct static mapping with full type" in {
    val cw = CrosswalkMapping("configuration/sources/testType", "testValue", dynamic = false)
    val s = cw.mkString

    val expected = "{\"type\": \"configuration/sources/testType\", \"value\": \"testValue\"}"
    assert(s.equals(expected))
  }

  it should "produce correct dynamic mapping" in {
    val cw = CrosswalkMapping("col", "testValue", dynamic = true)
    val s = cw.mkString

    val expected = """{"column": "col", "value": "testValue"}"""
    assert(s.equals(expected))
  }

  "Member Mapping" should "produce correct mapping" in {
    val cw = CrosswalkMapping("testType", "testValue")
    val mm = MemberMapping(memberType = "HCP", cw)
    val s = mm.mkString()

    val expected = """"HCP": {"crosswalks": [{"type": "configuration/sources/testType", "value": "testValue"}]}"""
    assert(s.equals(expected))
  }

  "Attribute Mapping" should "produce correct mapping" in {
    val am = AttributeMapping("attr", Seq("field1", "field2"))
    val s = am.mkString()

    val expected = """"attr": [{"value": "field1"}, {"value": "field2"}]"""
    assert(s.equals(expected))
  }

  "Mapping" should "produce correct mapping" in {
    val m = Mapping(
      interactionType = "intType",
      crosswalk = CrosswalkMapping("intCW", "intValue"),
      members = Seq(
        MemberMapping(
          memberType = "HCP",
          CrosswalkMapping("memberCW", "memberValue")
        )
      ),
      attributes = Seq(
        AttributeMapping("attr1", Seq("field")),
        AttributeMapping("attr2", Seq("field"))
      )
    )

    val s = m.mkString()

    val expected =
      """{
        |  "type" : "configuration/interactionTypes/intType",
        |  "crosswalks" : [ {
        |    "type" : "configuration/sources/intCW",
        |    "value" : "intValue"
        |  } ],
        |  "members" : {
        |    "HCP" : {
        |      "crosswalks" : [ {
        |        "type" : "configuration/sources/memberCW",
        |        "value" : "memberValue"
        |      } ]
        |    }
        |  },
        |  "attributes" : {
        |    "attr1" : [ {
        |      "value" : "field"
        |    } ],
        |    "attr2" : [ {
        |      "value" : "field"
        |    } ]
        |  }
        |}""".stripMargin

    assert(s.equals(expected))
  }

}
