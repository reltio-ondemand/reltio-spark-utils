package com.reltio.util.unzip

import java.io.{File, InputStream}

import com.amazonaws.services.s3.model.PutObjectResult
import org.apache.commons.io.IOUtils
import org.apache.spark.rdd.RDD
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

import scala.collection.convert.wrapAsScala._

class UnzipUtilsTest extends SparkTestBase {
  "Unzip Utils" should "unzip file correctly" in {
    val file = "file.zip"
    val is: InputStream = classOf[UnzipUtils].getClassLoader.getResourceAsStream(s"$file")
    val s = Unzipper.unzip(is)

    assert(s.get.size == 1)
    assert(s.get(0)._2.length > 0)
  }

  "Unzip Step" should "produce correct results" in {
    val s3Config = S3Conf(
      awsKey = "AAA",
      awsSecret = "BBB",
      bucket = "bucket",
      outBucket = None
    )
    val file = "file.zip"

    val uz = spy(new UnzipUtils(sc.get, s3Config))
    val s3Mock = mock(classOf[S3Client])

    val is: InputStream = getClass.getClassLoader.getResourceAsStream(s"$file")

    when(s3Mock.getS3Object(anyString())).thenAnswer(new Answer[InputStream] {
      override def answer(invocation: InvocationOnMock): InputStream = {
        println("Mock S3 Client")
        is
      }
    })
    when(s3Mock.saveS3Object(anyString(), any(classOf[String]))).thenReturn(new PutObjectResult())
    //doReturn(s3Mock).when(uz).s3clientF(any(classOf[S3Config]))

    uz.s3Client = s3Mock

    val files = Seq(file)
    val res = uz.doUnzip(files, 2, "output")

    assert(res(0)._2 == "success")
  }

  def filesRdd: RDD[(String, String)] = {
    require(sc.isDefined)
    require(sqlc.isDefined)

    val name = "entities-ids.json.zip"

    val stream : InputStream = getClass.getClassLoader.getResourceAsStream(s"$name")
    val content = IOUtils.readLines(stream, "UTF-8").mkString("\n")

    stream.close()

    sc.get.parallelize(Seq((name, content)))
  }
}
