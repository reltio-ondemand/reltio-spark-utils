package com.reltio.util.unzip

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.scalatest.FlatSpec


trait SparkTestBase extends FlatSpec{
    var sc: Option[SparkContext] = None
    var sqlc: Option[SQLContext] = None

    val delim = ","

    def sparkContext() = sc

    def setup() = {
      val sparkConf: SparkConf = new SparkConf()
        .setAppName("spark-test-rollup")
        .setMaster("local[2]")
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

      sc = Option(new SparkContext(sparkConf))
      sqlc = Option(new SQLContext(sc.get))
    }

    def teardown() = {
      if (Option(sc).isDefined){
        sc.get.stop()
        sc = None
        sqlc = None
      }
    }

    override def withFixture(test: NoArgTest) = {
      setup()
      try {
        super.withFixture(test)
      } finally {
        teardown()
      }
    }
}
