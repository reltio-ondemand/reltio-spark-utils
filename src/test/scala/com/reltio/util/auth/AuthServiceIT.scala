package com.reltio.util.auth

import org.scalatest.FlatSpec

/**
 * Created by maxlukichev on 3/30/17.
 */
class AuthServiceIT extends FlatSpec {

  val user = System.getenv("reltioUsername")
  val apiKey = System.getenv("reltioKey")
  val passw = System.getenv("reltioPassword")
  val authUrl = "https://auth-stg.reltio.com/oauth/token"

  "Authenticate" should "return valid token given username and password" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.authenticate(user, passw)

    assert(token.length > 0)
  }

  it should "return valid refresh token given username and password" in {
    val auth = new AuthService(authUrl, apiKey)
    val token =  auth.refreshToken(user, passw)

    assert(token.length > 0)
  }

  it should "return valid token given refresh token" in {
    val auth = new AuthService(authUrl, apiKey)
    val refreshToken =  auth.refreshToken(user, passw)
    val token = auth.authenticate(refreshToken)

    assert(token.length > 0)
  }

}
