package com.reltio.util.metadata

import com.reltio.util.auth.AuthService
import org.scalatest.FlatSpec

/**
 * Created by maxlukichev on 3/23/17.
 */
class ReltioMetadataServiceIT extends FlatSpec {
  "AddAttributes" should "add new attributes to the metadata" in {

    //This should be passed
    val tenant = sys.props.getOrElse("ri.test.tenant", "")
    val url = sys.props.getOrElse("ri.test.url", "")
    val apiKey = sys.props.getOrElse("ri.test.apiKey", "")
    val username = sys.props.getOrElse("ri.test.username", "")
    val passw = sys.props.getOrElse("ri.test.passw", "")

    val auth = new AuthService(url, apiKey)
    val refreshToken = auth.refreshToken(username, passw)
    val token = auth.authenticate(refreshToken)

    val mu = new ReltioMetadataService(url: String, token: String, tenant: String)
    //TODO:
  }

  it should "not update metadata if same attributes already exist" in {
    //TODO:
  }
}
