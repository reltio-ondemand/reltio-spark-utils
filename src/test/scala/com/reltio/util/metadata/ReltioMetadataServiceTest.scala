package com.reltio.util.metadata

import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.FlatSpec

import scala.io.Source

/**
 * Created by maxlukichev on 10/26/16.
 */
class ReltioMetadataServiceTest extends FlatSpec {

  def metadataService: ReltioMetadataService = {
    val is = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val metadata = Source.fromInputStream(is).getLines().mkString("\n")

    val ms = spy(new ReltioMetadataService("a", "b", "c"))
    doReturn(metadata).when(ms).metadataString()
    doReturn("").when(ms).updateMetadata(anyString())

    ms
  }

  "MetadataService" should "add attributes correctly" in {
    val entities = Seq("Type1", "Type2")

    /*val template =
      """
        |{   "uri": "#entityType_uri#/analyticsAttributes/#atrName#",
        |        "name": "#atrName#",
        |        "label": "#atrName#",
        |        "type": "Number",
        |        "faceted": true,
        |        "searchable": true
        |}
      """.stripMargin*/

    val template =
      """
        |{
        |        "name": "#atrName#"
        |}
      """.stripMargin

    val res = ReltioMetadataService.constructMetadata(
      List("Type1"),
      List("newAttr1", "newAttr2"),
      metadataService.metadataString(),
      template,
      ReltioMetadataService.ATTRIBUTES)

    println(res)

    val m1is = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-result.json")
    val m1 = Source.fromInputStream(m1is).getLines().mkString("").replaceAll("\\s", "")
    m1is.close()

    assert(res == m1)
  }

  it should "detect two identical metadatas are same" in {
    val m1 = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val m1String = Source.fromInputStream(m1).getLines().mkString("\n")
    m1.close()

    val m2 = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val m2String = Source.fromInputStream(m2).getLines().mkString("\n")
    m2.close()

    assert(!ReltioMetadataService.checkMetadataChanged(m1String, m2String))
  }

  it should "detect added attributes (analytic and regular)" in {
    val m1 = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val oldMd = Source.fromInputStream(m1).getLines().mkString("\n")
    m1.close()

    val at = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("attribute-template.json")
    val template = Source.fromInputStream(at).getLines().mkString("\n")
    at.close()

    val newMd1 = ReltioMetadataService.constructMetadata(
      entities = List("Type1"),
      attributes = List("attr2"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ATTRIBUTES
    )

    assert(ReltioMetadataService.checkMetadataChanged(oldMd, newMd1))

    val newMd2 = ReltioMetadataService.constructMetadata(
      entities = List("Type1"),
      attributes = List("attrA2"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ANALYTICS_ATTRIBUTES
    )

    assert(ReltioMetadataService.checkMetadataChanged(oldMd, newMd2))
  }

  it should "detect updated attributes (analytic and regular)" in {
    val m1 = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val oldMd = Source.fromInputStream(m1).getLines().mkString("\n")
    m1.close()

    val template =
      """
        |{
        |        "name": "#atrName#",
        |        "changed": "true"
        |}
      """.stripMargin

    val newMd1 = ReltioMetadataService.constructMetadata(
      entities = List("Type1"),
      attributes = List("attr1"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ATTRIBUTES
    )

    assert(ReltioMetadataService.checkMetadataChanged(oldMd, newMd1))

    val newMd2 = ReltioMetadataService.constructMetadata(
      entities = List("configuration/entityTypes/Type1"),
      attributes = List("attrA1"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ANALYTICS_ATTRIBUTES
    )

    assert(ReltioMetadataService.checkMetadataChanged(oldMd, newMd2))
  }


  it should "not detect change if attributes didn't actually change" in {
    val m1 = classOf[ReltioMetadataServiceTest].getClassLoader.getResourceAsStream("metadata-sample.json")
    val oldMd = Source.fromInputStream(m1).getLines().mkString("\n")
    m1.close()

    var template =
      """
        |{
        |        "name": "#atrName#"
        |}
      """.stripMargin


    val newMd1 = ReltioMetadataService.constructMetadata(
      entities = List("Type1"),
      attributes = List("attr1"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ATTRIBUTES
    )

    assert(!ReltioMetadataService.checkMetadataChanged(oldMd, newMd1))

    val newMd2 = ReltioMetadataService.constructMetadata(
      entities = List("Type1"),
      attributes = List("attrA1"),
      metadata = oldMd,
      attributeTemplate = template,
      attributeType = ReltioMetadataService.ANALYTICS_ATTRIBUTES
    )

    assert(!ReltioMetadataService.checkMetadataChanged(oldMd, newMd2))
  }

}
