package com.reltio.util

import com.reltio.util.exception.ReltioApiException

import scalaj.http.{HttpOptions, HttpResponse, Http}
import org.json4s.JsonAST.JValue
import org.json4s.jackson.JsonMethods

class RestUtil {

  def postRequest(url: String, headers: Map[String, String], formData: Seq[(String, String)]): JValue = {
    val response: HttpResponse[String] = Http(url)
      .headers(headers)
      .option(HttpOptions.allowUnsafeSSL)
      .postForm(formData)
      .asString

    val  code = response.code
    if (code != 200)
      throw new ReltioApiException(s"The api call failed with response status: $code")

    val json = JsonMethods.parse(response.body)
    json
  }

  def postRequest(url: String, headers: Map[String, String], content: String): JValue = {
    val response: HttpResponse[String] = Http(url)
      .headers(headers)
      .option(HttpOptions.allowUnsafeSSL)
      .postData(content)
      .asString

    val  code = response.code
    if (code != 200)
      throw new ReltioApiException(s"The api call failed with response status: $code")

    val json = JsonMethods.parse(response.body)
    json
  }

  def getRequest(url: String, headers: Map[String, String]): JValue = {

    val response: HttpResponse[String] = Http(url)
      .headers(headers)
      .option(HttpOptions.allowUnsafeSSL)
      .asString

    val  code = response.code
    if (code != 200)
      throw new ReltioApiException(s"The api call failed with response status: $code")

    val json = JsonMethods.parse(response.body)
    json
  }
}

object RestUtil {

  def postRequest(url: String, headers: Map[String, String], formData: Seq[(String, String)]): JValue = {
    new RestUtil().postRequest(url, headers, formData)
  }

  def postRequest(url: String, headers: Map[String, String], content: String): JValue = {
    new RestUtil().postRequest(url, headers, content)
  }

  def getRequest(url: String, headers: Map[String, String]): JValue = {
    new RestUtil().getRequest(url, headers)
  }
}
