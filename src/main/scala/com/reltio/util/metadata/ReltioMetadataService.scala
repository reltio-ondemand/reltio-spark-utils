package com.reltio.util.metadata

import com.reltio.util.exception.ReltioApiException
import org.json4s._
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.Serialization.write

import scalaj.http.{Http, HttpOptions, HttpResponse}

/**
 * This service provides useful methods of updating Reltio metadata via Configuration REST API
 *
 * @author maxim.lukichev
 */
class ReltioMetadataService(url: String, token: String, tenant: String) {

  /**
   * Get tenant's L3 configuration
   * @return the metadata configuration in form of json
   */
  def metadataString(): String = {
    //val token = new AuthService(authUrl, apiKey).authenticate(refreshToken)

    val getUrl = s"$url/$tenant/configuration/_noInheritance"
    val getMetadata: HttpResponse[String] = Http(getUrl)
      .header("Authorization", s"Bearer $token")
      .option(HttpOptions.allowUnsafeSSL)
      .asString

    val code = getMetadata.code
    if (code != 200)
      throw new ReltioApiException(s"Can not retrieve metadata: api call failed with response status: $code")

    getMetadata.body
  }

  /**
   * Apply given metadata as new L3 configuration for the tenant
   * @param metadata the metadata string to apply
   * @return the body of the post response
   */
  def updateMetadata(metadata: String) = {
    val updateUrl = s"$url/$tenant/configuration"
    val updateMetadata: HttpResponse[String] = Http(updateUrl)
      .headers(
        ("Authorization", s"Bearer $token"),
        ("Content-Type", "application/json")
      )
      .option(HttpOptions.allowUnsafeSSL)
      .postData(metadata)
      .method("PUT")
      .asString

    val code = updateMetadata.code
    if (code != 202) //202 = accepted
      throw new ReltioApiException(s"Can not update metadata: api call failed with response status: $code")

    updateMetadata.body
  }

  /**
   * Updtates tenant's L3 configuration by adding new attributes
   * @param entities the list of entities to add the attributes to
   * @param attributes the list of attribute names to generate
   * @param attributeTemplate the attribute template to use when adding new attributes
   * @param attributeType the attribute type: 'analyticsAttributes' or 'attributes'
   */
  def addAttributes(entities: List[String], attributes: List[String], attributeTemplate: String, attributeType: String): Unit = {
    val metadata = metadataString()

    val newMetadata = ReltioMetadataService.constructMetadata(entities, attributes, metadata, attributeTemplate, attributeType)

    if (ReltioMetadataService.checkMetadataChanged(metadata, newMetadata)) {
      //update metadata only if it in fact changed
      updateMetadata(newMetadata)
    }
  }
}

object ReltioMetadataService {
  private val MISSING_URI = "none"
  private[metadata] val ANALYTICS_ATTRIBUTES = "analyticsAttributes"
  private[metadata] val ATTRIBUTES = "attributes"


  /**
   * Generate metadata for given list of attribute names using provided attributeTemplate
   * @param attributeTemplate the template of attribute to add
   * @param attributeNames the names of the attributes to add
   * @return
   */
  private[metadata] def attributesMetadataString(
                                                  attributeTemplate: String,
                                                  attributeNames: Seq[String]): String = {
    val as = attributeNames.map(a => attributeTemplate.replaceAll("#atrName#", a)).mkString(",")
    if (as.length > 0){
      "[" + as + "]"
    } else {
      as
    }
  }

  private[metadata] def entityTypeUri(entMetadata: Map[String, Any]): String = {
    entMetadata.getOrElse("uri", MISSING_URI).asInstanceOf[String]
  }

  private[metadata] def attributeNames(entMetadata: Map[String, Any], attributeType: String): Set[String] = {
    val attributes = entMetadata.getOrElse(attributeType, List[Map[String, Any]]()).asInstanceOf[List[Map[String, Any]]]
    attributes.map(a => a.get("name")).filter(_.isDefined).map(a => a.get.asInstanceOf[String]).toSet
  }

  private[metadata] def findEntityTypeSection(entType: String, entMetadata: List[Map[String, Any]]): Option[Map[String, Any]] = {
    entMetadata.find(et => entityTypeUri(et) == entType)
  }

  private[metadata] def attrEquals(l: Map[String, Any], r: Map[String, Any]): Boolean = {
    l.size == r.size &&
    l.forall(e => {
      val key = e._1
      val value = e._2
      r.get(key).isDefined && r.get(key).get == value
    })
  }

  private[metadata] def typeAttributesMatch(l: List[Map[String, Any]], r: List[Map[String, Any]]): Boolean = {
    l.size == r.size &&
      l.forall(a => {
        r.exists(n => attrEquals(a,n))
      })
  }

  private[metadata] def checkMetadataChanged(oldMetadata: String, newMetadata: String): Boolean = {


    val oldMetaMap = parseMetadata(oldMetadata)
    val newMetaMap = parseMetadata(newMetadata)


    val typeNames = oldMetaMap.getOrElse("entityTypes", List()).asInstanceOf[List[Map[String, Any]]] //convert to a list of maps
      .map(t => entityTypeUri(t)) //get entity type uris
      .filter(t => t != MISSING_URI) //filter out those which are missing

    val otypes = oldMetaMap.getOrElse("entityTypes", List()).asInstanceOf[List[Map[String, Any]]]
    val ntypes = newMetaMap.getOrElse("entityTypes", List()).asInstanceOf[List[Map[String, Any]]]

    //Below is inefficient nested-loop implementation, but it's ok since the number of types is very small

    //get pairs of types to compare
    val tpairs = typeNames.map(t => (findEntityTypeSection(t, otypes), findEntityTypeSection(t, ntypes)))
    //validate all the pairs
    if (!tpairs.forall(p => p._1.isDefined && p._2.isDefined)){
      //TODO: add logging
      throw new RuntimeException("New and old metadata has different set of entity types")
    }

    val m = tpairs.map(p => {
      val ot = p._1.get
      val nt = p._2.get

      val oldAttrs = ot.get(ATTRIBUTES)
      val newAttrs = nt.get(ATTRIBUTES)
      val attributesMatch = oldAttrs.isDefined && newAttrs.isDefined && typeAttributesMatch(
        oldAttrs.get.asInstanceOf[List[Map[String, Any]]],
        newAttrs.get.asInstanceOf[List[Map[String, Any]]]
      )

      val oldAAttrs = ot.get(ANALYTICS_ATTRIBUTES)
      val newAAttrs = nt.get(ANALYTICS_ATTRIBUTES)
      val analyticAttributesMatch =
        if (oldAAttrs.isEmpty && newAAttrs.isEmpty) {
          true
        } else {
          oldAAttrs.isDefined && newAAttrs.isDefined && typeAttributesMatch(
            oldAAttrs.get.asInstanceOf[List[Map[String, Any]]],
            newAAttrs.get.asInstanceOf[List[Map[String, Any]]]
          )
        }
      //return true if both attributes and analytic attributes are the same
      attributesMatch && analyticAttributesMatch
    })

    !m.forall(_ == true)
  }

  /**
   * Generate new entity metadata
   * @param attributeTemplate the template to use to generate attributes
   * @param attributeNames the attributes to generate
   * @param uri the entity type uri
   * @param entity the entity metadata map
   * @param attributeType the kind of attributes to generate, i.e. analytics or regular
   */
  private[metadata] def addEntityAttributes(
                                             attributeTemplate: String,
                                             attributeNames: Seq[String],
                                             uri: String,
                                             entity: Map[String, Any],
                                             attributeType: String): Map[String, Any] = {
    require(attributeType == ReltioMetadataService.ATTRIBUTES || attributeType == ReltioMetadataService.ANALYTICS_ATTRIBUTES,
      s"Unknown attribute type [$attributeType]")

    //new attributes configuration
    val attributesMetadata  = attributesMetadataString(attributeTemplate, attributeNames)

    // replace generic w/ actual entity type uri
    val am  = attributesMetadata.replaceAll("#entityType_uri#", uri)
    // attributes to be added
    val newAttrs = JsonMethods.parse(am).values.asInstanceOf[List[Map[String, Any]]]

    val curAttrs = entity.getOrElse(attributeType, List[Map[String, Any]]()).asInstanceOf[List[Map[String, Any]]]
    //remove those attributes which are already in newAttrs list
    val newAttrNames = newAttrs.map(x => x.get("name")).filter(x => x.isDefined).map(x => x.get).toSet
    val filteredCurAttrs = curAttrs
      .filter(x => x.get("name").isDefined)
      .filter(x => {
        val name = x.get("name").get
        !newAttrNames.contains(name)
    })

    //concatenate old and new attributes
    val attrs = filteredCurAttrs ++ newAttrs

    entity + (attributeType -> attrs) // overwrite corresponding attribute section
  }

  private[metadata] def parseMetadata(metadata: String): Map[String, Any] = {
    val metaJ = JsonMethods.parse(metadata)
    metaJ.values.asInstanceOf[Map[String, Any]]
  }

  /**
   * Modifies given metadata string to add new attributes for the specified entity types
   *
   * @param entities the list of entity types to extend with attributes
   * @param attributes the list of attributes to extend with
   * @param metadata the metadata to modify
   * @param attributeTemplate the template of attribute metadata
   * @return new metadata string with the attributes added into it
   */
  private[metadata] def constructMetadata(
                                           entities: List[String],
                                           attributes: List[String],
                                           metadata: String,
                                           attributeTemplate: String,
                                           attributeType: String): String = {


    val metaMap = parseMetadata(metadata)
    val entTypes: List[Map[String, Any]] = metaMap.getOrElse("entityTypes", List()).asInstanceOf[List[Map[String, Any]]]

    val ents = entities.map {
      case e@s if s.startsWith("configuration/entityTypes/") => e
      case e => s"configuration/entityTypes/$e"
    }

    // build new entityTypes Config
    val newEntTypes = entTypes.map(e => {
      val uri = e.getOrElse("uri", MISSING_URI).asInstanceOf[String]
      if (ents.contains(uri) && uri != MISSING_URI){
        addEntityAttributes(attributeTemplate, attributes, uri, e, attributeType)
      } else {
        e
      }
    })

    // update entityTypes config map
    val newMeatadataMap = metaMap + ("entityTypes" -> newEntTypes)

    // convert scala collection into string
    implicit val formats = DefaultFormats
    write(newMeatadataMap)  // updated meta config
  }

}
