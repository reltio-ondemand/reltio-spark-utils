package com.reltio.util

import java.io.File

import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.{AmazonServiceException, AmazonClientException}
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import org.apache.commons.io.IOUtils

/**
 *
 * Simple client to abstract operations with AWS S3
 *
 * @author maxim.lukichev
 */
class AwsClient(val awsKey: String, val awsSecret: String, val region: Regions = Regions.US_EAST_1) {

  private lazy val userAwsCredentials = new BasicAWSCredentials(awsKey, awsSecret)
  private lazy val userS3Client = new AmazonS3Client(userAwsCredentials)
  userS3Client.setRegion(com.amazonaws.regions.Region.getRegion(region))

  def bucketName(url: String): String = {
    //s3://test.bucket/folder1/
    val s = url.replaceFirst("s3://", "")
    s.substring(0, s.indexOf("/"))
  }

  def fileLoc(url: String): String = {
    url.replaceFirst("s3://", "").replaceFirst(bucketName(url) + "/", "")
  }

  def awsCommand(resource: String, f : => Option[Any]): Option[Any] = {
    try {
      f
    } catch {
      case e: AmazonClientException =>
        println("Problem accessing AWS: " + e.getMessage)
        throw e
      case e: AmazonServiceException =>
        e.getStatusCode match {
          case 404 => println(s"[$resource] doesn't exist")
          case 403 => println(s"User doesn't have permissions to [$resource]")
        }
        throw e
    }
  }

  def prepareUpload(url: String ) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    //check if bucket exists
    awsCommand(
    "bucket",
    {
      if (!userS3Client.doesBucketExist(bucket)) {
        userS3Client.createBucket(bucket)
      }
      None
    })

    //check if object exists and delete it if so
    awsCommand(
    "object",
    {
      //if (userS3Client.doesObjectExist(bucket, fileKey)) {
      try {
        userS3Client.deleteObject(bucket, fileKey)
      } catch {
        case e: Throwable => None //do nothing
      }

      //}
      None
    })
  }

  /**
   * Update file's content on S3 bucket
   * @param content the file's content
   * @param url the file's location on S3
   * @return
   */
  def updateFileContent(content: String, url: String ) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    prepareUpload(url)

    //upload new file
    awsCommand(
    "object", {
      val od = new ObjectMetadata()
      userS3Client.putObject(bucket, fileKey, IOUtils.toInputStream(content, "UTF-8"), od)
      //userS3Client.putObject(bucket, fileKey, content) //version 1.11.X
      None
    })
  }

  def uploadFile(file: String, url: String) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    prepareUpload(url)

    //upload new file
    awsCommand(
    "object", {
      userS3Client.putObject(bucket, fileKey, new File(file))
      None
    })
  }

  def downloadFile(url: String): String = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    awsCommand(
    "object", {
      Option(IOUtils.toString(userS3Client.getObject(bucket, fileKey).getObjectContent))
    }).get.toString
  }

  def deleteFile(url: String) = {
    val bucket = bucketName(url)
    val fileKey = fileLoc(url)

    awsCommand(
    "object", {
      userS3Client.deleteObject(bucket, fileKey)
      None
    })
  }
}

object AwsClient {
  def apply(awsKey: String, awsSecret: String, region: Regions = Regions.US_EAST_1) = {
    new AwsClient(awsKey, awsSecret, region)
  }
}
