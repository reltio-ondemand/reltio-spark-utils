package com.reltio.util.ri

import org.json4s.jackson.JsonMethods

case class Mapping(
                  interactionType: String,
                  crosswalk: CrosswalkMapping,
                  members: Seq[MemberMapping],
                  attributes: Seq[AttributeMapping]
                    ) {

  def mkString(): String = {
    val crosswalkEl: String = crosswalk.mkString
    val crosswalkSt: String = s"""\"crosswalks\": [$crosswalkEl]"""

    val membersList: String = members.map(_.mkString).mkString(", ")
    val membersSt: String = s"""\"members\": {$membersList}"""

    val attributesList: String = attributes.map(_.mkString).mkString(", ")
    val attributesSt: String = s"""\"attributes\": {$attributesList}"""

    val typeSt: String = s"""\"type\": \"configuration/interactionTypes/$interactionType\""""

    val s = s"{$typeSt, $crosswalkSt, $membersSt, $attributesSt}"

    JsonMethods.pretty(JsonMethods.parse(s))
  }
}

case class CrosswalkMapping(cwType: String, cwField: String, dynamic: Boolean = false) {
  def mkString(): String = {
    dynamic match {
      case true => s"""{\"column\": \"$cwType\", \"value\": \"$cwField\"}"""
      case false => {
        val fullType = cwType match {
          case s if s.startsWith("configuration/sources/") => s
          case _ => "configuration/sources/" + cwType
        }

        s"""{\"type\": \"$fullType\", \"value\": \"$cwField\"}"""
      }
    }
  }
}

case class AttributeMapping(attribute: String, fields: Seq[String]){
  def mkString(): String = {
    val fieldsS = fields.map(x=>s"""{\"value\": \"$x\"}""").mkString(", ")
    s"""\"$attribute\": [$fieldsS]"""
  }
}

case class MemberMapping(memberType: String, crosswalk: CrosswalkMapping){
  def mkString(): String = {
    val crosswalkEl = crosswalk.mkString
    val crosswalkSt = s"""\"crosswalks\": [$crosswalkEl]"""

    s"""\"$memberType\": {$crosswalkSt}"""
  }
}

