package com.reltio.util.ri

case class InputData(header: List[String], lines: List[InputRow]) {

  def mkString(): String = {
    val headerS = header.mkString(", ")
    val dataS = lines.map(_.mkString()).mkString("\n")

    s"$headerS\n$dataS"
  }

}

case class InputRow(data: List[String]) {
  def mkString(): String = {
    data.mkString(", ")
  }
}