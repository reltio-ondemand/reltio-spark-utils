package com.reltio.util.ri

import java.text.SimpleDateFormat
import java.util.Calendar

import com.reltio.util.{AwsClient, RestUtil}
import org.json4s.jackson.JsonMethods

class RiClient(apiUrl: String, token: String) {
  val jobApiUrl = apiUrl + "/api/v1.0/jobs"
  val headers = Map(
    ("Authorization", s"Bearer $token"),
    ("Content-Type", "application/json")
  )

  /**
   * Submit data import job providing mapping as object
   * @param tenant
   * @param input
   * @param mapping
   * @param awsKey
   * @param awsSecret
   * @param cluster
   * @return
   */
  def submitDataImportJob(
                           tenant: String,
                           input: String,
                           mapping: Mapping,
                           awsKey: String,
                           awsSecret: String,
                           cluster: Int): String = {

    val mappingS = mapping.mkString
    val mappingLoc = uploadMapping(input, mappingS, awsKey, awsSecret)
    submitDataImportJob(tenant, input, mappingLoc, awsKey, awsSecret, cluster)
  }

  /**
   * Sumbit data import job providing input data and mapping as objects
   * @param tenant
   * @param input
   * @param inputData
   * @param mapping
   * @param awsKey
   * @param awsSecret
   * @return
   */
  def submitDataImportJob(
                           tenant: String,
                           input: String,
                           inputData: InputData,
                           mapping: Mapping,
                           awsKey: String,
                           awsSecret: String): String = {

    val inputS = input.mkString
    val inputLoc = uploadFile(input, inputS, awsKey, awsSecret)

    val mappingS = mapping.mkString
    val mappingLoc = uploadMapping(input, mappingS, awsKey, awsSecret)
    
    submitDataImportJob(tenant, inputLoc, mappingLoc, awsKey, awsSecret, 1) //always runs on smallest cluster
  }

  private[util] def uploadFile(path: String,
                                 content: String,
                                 awsKey: String,
                                 awsSecret: String): String = {
    val awsClient = AwsClient(awsKey, awsSecret)

    awsClient.updateFileContent(content, "s3://" + path)

    path
  }
  
  private[util] def uploadMapping(input: String,
                                   mapping: String,
                                   awsKey: String,
                                   awsSecret: String): String = {

    val mappingLoc = RiClient.mappingPath(input)
    val awsClient = AwsClient(awsKey, awsSecret)

    awsClient.updateFileContent(mapping, "s3://" + mappingLoc)

    mappingLoc
  }

  def buildDataImportBody(
                           tenant: String,
                           input: String,
                           mapping: String,
                           awsKey: String,
                           awsSecret: String,
                           cluster: Int): String = {
    """
    {
        "name":"Data import",
        "tenant":"<tenant>",
        "tasks": [
            {
                "application": "DataImport",
                "payload": {
                    "inputPath" : "s3n://<input>",
                    "inputMappingPath" : "s3n://<mapping>",
                    "awsKey": "<awsKey>",
                    "awsSecret": "<awsSecret>",
                    "inactive" : "true",
                    "dryRun" : "false"
                }
            }
        ],
        "cluster": {
            "size": <size>
        }
    }
                   """
      .replaceAll("<tenant>",tenant)
      .replaceAll("<awsKey>", awsKey)
      .replaceAll("<awsSecret>", awsSecret)
      .replaceAll("<input>", input)
      .replaceAll("<mapping>", mapping)
      .replaceAll("<size>", cluster.toString)
  }

  /**
   * Submit data import job
   * @param tenant the tenant
   * @param input the input file s3 path
   * @param mapping the mapping file s3 path
   * @param awsKey the aws key
   * @param awsSecret the aws secret
   * @param cluster the size of the cluster to run the job on
   * @return
   */
  def submitDataImportJob(
                           tenant: String,
                           input: String,
                           mapping: String,
                           awsKey: String,
                           awsSecret: String,
                           cluster: Int): String = {

    val content = buildDataImportBody(tenant, input, mapping, awsKey, awsSecret, cluster)

    println(s"Starting import job: " + jobApiUrl)
    println(content)

    val job = RestUtil.postRequest(jobApiUrl, headers, content)
    val id = job \ "id" toString

    println("Started import job: " + id)
    id
  }

  def getJob(jobId: String): String = {

    val job = RestUtil.getRequest(jobApiUrl + s"/$jobId", headers)

    val pretty = JsonMethods.pretty(job)
    println(pretty)

    pretty
  }

  def listJobs(active: Boolean): String = {
    val job = RestUtil.getRequest(jobApiUrl + s"?active=$active", headers)

    val pretty = JsonMethods.pretty(job)
    println(pretty)

    pretty
  }

}

object RiClient {
  private[util] def mappingPath(inputPath: String): String = {
    val ind = inputPath.lastIndexOf('.')
    val pref = inputPath.substring(0, ind)

    val now = Calendar.getInstance().getTime()
    val format = new SimpleDateFormat("YY-MM-dd-hh:mm")
    val ts = format.format(now)
    pref + s"-$ts-mapping.json"
  }
}