package com.reltio.util.auth

import com.reltio.util.RestUtil
import org.json4s.DefaultFormats

/**
 * The helper service to get access token by given refreshToken
 *
 * @author maxim.lukichev
 */
class AuthService(authUrl: String, apiKey: String) {
    val authHeaders = Map("Authorization" -> s"Basic $apiKey") //default api key: cmVsdGlvX3VpOm1ha2l0YQ==
    implicit val formats = DefaultFormats

    def authenticate(refreshToken: String): String = {
      val json = RestUtil.postRequest(authUrl, authHeaders, Seq(("grant_type","refresh_token"), ("refresh_token",refreshToken)))
      val atJson = json \ "access_token"
      atJson.extract[String]
    }

    def authenticate(username: String, passw: String): String = {
      val json = RestUtil.postRequest(authUrl, authHeaders, Seq(("grant_type","password"), ("username",username),("password",passw)))
      val atJson = json \ "access_token"
      atJson.extract[String]
    }

    def refreshToken(username: String, passw: String): String = {
      val json = RestUtil.postRequest(authUrl, authHeaders, Seq(("grant_type","password"), ("username",username),("password",passw)))
      val atJson = json \ "refresh_token"
      atJson.extract[String]
    }

}
