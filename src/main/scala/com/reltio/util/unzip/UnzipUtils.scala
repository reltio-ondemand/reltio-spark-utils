package com.reltio.util.unzip

import java.io._
import java.util.zip.ZipInputStream

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import org.apache.commons.io.IOUtils
import org.apache.spark.SparkContext

import scala.collection.convert.wrapAsScala._
import scala.io.{Codec, Source}

/**
 * @author maxim.lukichev
 */
class UnzipUtils(
                  val sc: SparkContext,
                  val config: S3Conf) extends Serializable{

  def awsKey = config.awsKey
  def awsSecret = config.awsSecret
  def bucket = config.bucket

  def inputUrl(input: String) = s"s3://$awsKey:$awsSecret@$bucket/$input"

  def files(input: String): Seq[String] = {
    S3Client(config).listObjects(input).filter(x => x.endsWith(".zip"))
  }

  var s3Client = S3Client(config)

  def doUnzip(files: Seq[String], numPartitions: Int, output: String): Seq[(String, String)] = {
    val frdd = sc.parallelize(files, numPartitions)
    val broadcastClient = sc.broadcast(s3Client)
    val outputB = sc.broadcast(output)

    val res: Seq[(String, String)] = frdd.map(name => {

      def saveFiles(files: Seq[(String, String)]): String = {
        val saveRes = try {
          val outputVal = outputB.value
          files.map(x => {
            val fname = x._1
            val fcontent = x._2

            val outputKey = s"$outputVal/$fname"
            broadcastClient.value.saveS3Object(outputKey, fcontent)
          })
          "success"
        } catch {
          case e: Throwable => "failed to save: " + e.getMessage + "Casused by: " + e.getCause.getMessage
        }
        saveRes
      }

      def unzipFile(name: String): Option[Seq[(String, String)]] = {
        try {
          val client = broadcastClient.value
          val is = client.getS3Object(name)

          val bytes = IOUtils.toByteArray(is)
          val bis = new ByteArrayInputStream(bytes)

          val res = Unzipper.unzip(bis)

          bis.close()

          res
        } catch {
          case e: Exception => None
        }
      }

      val unzipped = unzipFile(name)
      val saveRes = if (unzipped.isDefined) {
        saveFiles(unzipped.get)
      } else {
        "failed to unzip"
      }
      (name, saveRes)
    }).collect()
      .map(x => (x._1, x._2))

    res.foreach(x => println(x._1 + " => " + x._2))
    res
  }
}

object UnzipUtils {
  /**
   * Unzip given collection of files and place them in the outuput folder
   * @param input the input folder/pattern
   * @param output the output folder name
   * @param sc SparkContext
   * @param s3Config the s3 access configuration
   * @param numPartitions the level of parallelism on Spark. Number of partitions
   */
  def unzipFiles(input: String, output: String, sc: SparkContext, s3Config: S3Conf, numPartitions: Int = 100): Unit = {
    val uz = new UnzipUtils(sc, s3Config)
    val files = uz.files(input)
    uz.doUnzip(files, numPartitions, output)
  }
}

case class S3Conf(awsKey: String, awsSecret: String, bucket: String, outBucket: Option[String]) {
  def outputBucket:String = {
    outBucket match {
      case b: Some[String] => b.get
      case None => bucket
    }
  }
}

case class S3Client(config: S3Conf) extends Serializable {

  def userS3Client = {
    val userAwsCredentials = new BasicAWSCredentials(config.awsKey, config.awsSecret)
    val uc = new AmazonS3Client(userAwsCredentials)
    uc.setRegion(com.amazonaws.regions.Region.getRegion(Regions.US_EAST_1))
    uc
  }

  def saveS3Object(key: String, content: String) = {
    val md = new ObjectMetadata()
    val sr = IOUtils.toInputStream(content)
    userS3Client.putObject(config.bucket, key, sr, md)
  }

  def getS3Object(key: String): InputStream = {
    userS3Client.getObject(config.bucket, key).getObjectContent()
  }

  def listObjects(prefix: String): Seq[String] = {
    userS3Client.listObjects(config.bucket, prefix).getObjectSummaries.map(x => x.getKey)
  }
}

object Unzipper {
  def unzip(is: InputStream): Option[Seq[(String, String)]] = {
    val fileNames = try {
      val zis = new ZipInputStream(is)
      var entry = zis.getNextEntry()
      var acc = List[(String, String)]()
      while (Option(entry).isDefined) {
        try {
          val lines = Source.fromInputStream(zis)(Codec.UTF8).getLines()
          val eName = entry.getName

          val content = lines.mkString("\n")
          if (content.length > 0){
            acc = acc :+ (eName, content)
          }
        } catch {
          case e: Exception => println("Failed to parse entry: " + entry.getName)
        }
        entry = zis.getNextEntry
      }
      Option(acc)
    } catch {
      case e: Exception => None
    } finally {

    }
    fileNames
  }
}